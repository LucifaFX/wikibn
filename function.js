'use strict';
$("#maggner-main-menu").menuify();
$("#maggner-main-menu .widget").addClass("show-menu");
$("#main-ad-top .widget").each(function () {
    var tarr = $(this);
    if (tarr.length) $("#ad-top").append(tarr).show()
});
$("#main-ad-footer .widget").each(function () {
    var tarr = $(this);
    if (tarr.length) $("#ad-footer").append(tarr).show()
});
$(".show-search").on("click", function () {
    $("#nav-search").fadeIn(170).find("input").focus()
});
$(".hide-search").on("click", function () {
    $("#nav-search").fadeOut(170).find("input").blur()
});
$(".follow-by-email-text").text(followByEmailText);
$(".Label a").attr("href", function (canCreateDiscussions, directory) {
    directory = directory.replace(directory, directory + "?&max-results=" + postPerPage);
    return directory
});
$(".avatar-image-container img").attr("src", function (canCreateDiscussions, originalBaseURL) {
    originalBaseURL = originalBaseURL.replace("//img1.blogblog.com/img/blank.gif", "//4.bp.blogspot.com/-oSjP8F09qxo/Wy1J9dp7b0I/AAAAAAAACF0/ggcRfLCFQ9s2SSaeL9BFSE2wyTYzQaTyQCK4BGAYYCw/s35-r/avatar.jpg");
    return originalBaseURL
});
$("#maggner-sidebar-tabs").each(function () {
    $("#maggner-sidebar-tabs .widget").each(function () {
        var classesLine = $(this).find(".widget-title > h3").text().trim();
        $(this).attr("tab-ify", classesLine)
    });
    $("#maggner-sidebar-tabs").tabify();
    var index = $("#maggner-sidebar-tabs .widget").length;
    if (index >= 1) $(this).addClass("tabs-" + index).show()
});
$(".share-links .window-ify").on("click", function () {
    var embedContainer = $(this);
    var url = embedContainer.data("url");
    var labelWidth = embedContainer.data("width");
    var imgH = embedContainer.data("height");
    var widthCanvas = window.screen.width;
    var cellCY = window.screen.height;
    var _0x630cxd = Math.round(widthCanvas / 2 - labelWidth / 2);
    var y = Math.round(cellCY / 2 - imgH / 2);
    var newwin = window.open(url, "_blank", "scrollbars=yes,resizable=yes,toolbar=no,location=yes,width=" + labelWidth + ",height=" + imgH + ",left=" + _0x630cxd + ",top=" +
        y);
    newwin.focus()
});
$(".share-links").each(function () {
    var verticalNavigation = $(this);
    var resizeOptionsTable = verticalNavigation.find(".show-hid a");
    resizeOptionsTable.on("click", function () {
        verticalNavigation.toggleClass("show-hidden")
    })
});
$(".post-body a").each(function () {
    var detailViewItem = $(this);
    var singleUri = detailViewItem.text().trim();
    var values = singleUri.split("/");
    var value3 = values[0];
    var type = values[1];
    var connectionColor = values.pop();
    if (singleUri.match("button")) {
        detailViewItem.addClass("button").text(value3);
        if (type != "button") detailViewItem.addClass(type);
        if (connectionColor != "button") detailViewItem.addClass("colored-button").css({
            "background-color": connectionColor
        })
    }
});
$(".post-body strike").each(function () {
    var $token = $(this);
    var container = $token.text().trim();
    var _0x630cx16 = $token.html();
    if (container.match("contact-form")) {
        $token.replaceWith('<div class="contact-form"/>');
        $(".contact-form").append($("#ContactForm1"))
    }
    if (container.match("alert-success")) $token.replaceWith('<div class="alert-message alert-success short-b">' + _0x630cx16 + "</div>");
    if (container.match("alert-info")) $token.replaceWith('<div class="alert-message alert-info short-b">' + _0x630cx16 + "</div>");
    if (container.match("alert-warning")) $token.replaceWith('<div class="alert-message alert-warning short-b">' + _0x630cx16 + "</div>");
    if (container.match("alert-error")) $token.replaceWith('<div class="alert-message alert-error short-b">' + _0x630cx16 + "</div>");
    if (container.match("left-sidebar")) $token.replaceWith("<style>.item #main-wrapper{float:right}.item #sidebar-wrapper{float:left}</style>");
    if (container.match("right-sidebar")) $token.replaceWith("<style>.item #main-wrapper{float:left}.item #sidebar-wrapper{float:right}</style>");
    if (container.match("full-width")) $token.replaceWith("<style>.item #main-wrapper{width:100%}.item #sidebar-wrapper{display:none}</style>");
    if (container.match("code-box")) $token.replaceWith('<pre class="code-box short-b">' + _0x630cx16 + "</pre>");
    var $words = $(".post-body .short-b").find("b");
    $words.each(function () {
        var $obfuscatedEmail = $(this);
        var container = $obfuscatedEmail.text().trim();
        if (container.match("alert-success") || container.match("alert-info") || container.match("alert-warning") || container.match("alert-error") ||
            container.match("code-box")) $obfuscatedEmail.replaceWith("")
    })
});
$(".about-author .author-description span a").each(function () {
    var $newmsgLink = $(this);
    var this_href = $newmsgLink.attr("href");
    if (this_href.match("facebook.com")) $newmsgLink.replaceWith('<li class="facebook-f"><a href="' + this_href + '" title="Facebook" target="_blank"/></li>');
    else if (this_href.match("twitter.com")) $newmsgLink.replaceWith('<li class="twitter"><a href="' + this_href + '" title="Twitter" target="_blank"/></li>');
    else if (this_href.match("google.com")) $newmsgLink.replaceWith('<li class="gplus"><a href="' + this_href +
        '" title="Google Plus" target="_blank"/></li>');
    else if (this_href.match("pinterest.com")) $newmsgLink.replaceWith('<li class="pinterest-p"><a href="' + this_href + '" title="Pinterest" target="_blank"/></li>');
    else if (this_href.match("instagram.com")) $newmsgLink.replaceWith('<li class="instagram"><a href="' + this_href + '" title="Instagram" target="_blank"/></li>');
    else if (this_href.match("linkedin.com")) $newmsgLink.replaceWith('<li class="linkedin"><a href="' + this_href + '" title="Linkedin" target="_blank"/></li>');
    else if (this_href.match("youtube.com")) $newmsgLink.replaceWith('<li class="youtube"><a href="' + this_href + '" title="YouTube" target="_blank"/></li>');
    else $newmsgLink.replaceWith('<li class="external-link"><a href="' + this_href + '" title="Site" target="_blank"/></li>');
    $(".author-description").append($(".author-description span li"));
    $(".author-description").addClass("show-icons")
});
$("#maggner-main-menu li").each(function () {
    var clicked = $(this);
    var $barcodelink = clicked.find("a");
    var this_href = $barcodelink.attr("href");
    var k = this_href.toLowerCase();
    var el = clicked;
    var value = el;
    var off = k;
    if (k.match("getmega")) el.addClass("has-sub mega-menu").append('<div class="getMega">' + this_href + "</div>");
    el.find(".getMega").shortcode({
        getMega: function () {
            var l = this.options.label;
            var eYp4 = this.options.type;
            var minuteFraction = 5;
            ajaxMega(el, eYp4, minuteFraction, l, off);
            if (eYp4 == "mtabs") {
                if (l != undefined) l =
                    l.split("/");
                megaTabs(value, eYp4, l)
            }
        }
    })
});
function megaTabs(container, p, fields) {
    if (p == "mtabs")
        if (fields != undefined) {
            var fieldsLen = fields.length;
            var photoText = '<ul class="complex-tabs">';
            var j = 0;
            for (; j < fieldsLen; j++) {
                var rg = fields[j];
                if (rg) photoText = photoText + ('<div class="mega-tab" tab-ify="' + rg + '"/>')
            }
            photoText = photoText + "</ul>";
            container.addClass("mega-tabs mtabs").append(photoText);
            container.find("a:first").attr("href", "#");
            $(".mega-tab").each(function () {
                var $deepPage = $(this);
                var sz2 = $deepPage.attr("tab-ify");
                ajaxMega($deepPage, "megatabs",
                    4, sz2, "getmega")
            });
            container.find("ul.complex-tabs").tabify({
                onHover: true
            })
        } else container.addClass("mega-tabs").append('<ul class="mega-widget">' + msgError() + "</ul>")
}
$(".maggner-widget-ready .HTML .widget-content").each(function () {
    var query = $(this);
    var TabularCtrl = query.text().trim().toLowerCase();
    query.shortcode({
        getWidget: function () {
            var groups = this.options.results;
            var match = this.options.label;
            var processType = this.options.type;
            ajaxWidget(query, processType, groups, match, TabularCtrl)
        },
        getInstagram: function () {
            var groups = this.options.results;
            var start = this.options.token;
            getInsta(query, groups, start, TabularCtrl)
        }
    })
});
$(".related-content").each(function () {
    var $unusedPanel = $(this);
    var name = $unusedPanel.find(".related-tag").attr("data-label");
    ajaxRelated($unusedPanel, "related", 3, name, "getrelated")
});
function msgError() {
    return '<span class="no-posts">Error: No Results Found.</span>'
}
function beforeLoader() {
    return '<div class="loader"/>'
}
function getFeedUrl(action, options, name) {
    var header = "";
    switch (name) {
    case "recent":
        header = "/feeds/posts/default?alt=json&max-results=" + options;
        break;
    case "comments":
        if (action == "list") header = "/feeds/comments/default?alt=json&max-results=" + options;
        else header = "/feeds/posts/default/-/" + name + "?alt=json&max-results=" + options;
        break;
    default:
        header = "/feeds/posts/default/-/" + name + "?alt=json&max-results=" + options;
        break
    }
    return header
}
function getPostLink(product, name) {
    var i = 0;
    for (; i < product[name].link.length; i++)
        if (product[name].link[i].rel == "alternate") {
            var entityUrl = product[name].link[i].href;
            break
        } return entityUrl
}
function getPostTitle(nextActionList, index) {
    var video1title = nextActionList[index].title.$t;
    return video1title
}
function getPostDate(res, i) {
    var summary = res[i].published.$t;
    var baseName = summary.substring(0, 4);
    var total_pageviews_raw = summary.substring(5, 7);
    var _0x630cx33 = summary.substring(8, 10);
    var middlePathName = monthFormat[parseInt(total_pageviews_raw, 10) - 1] + " " + _0x630cx33 + ", " + baseName;
    var _0x630cx22 = '<span class="post-date"><time class="published" datetime="' + summary + '">' + middlePathName + "</time></span>";
    return _0x630cx22
}
function getPostImage(a, i) {
    var id = a[i].content.$t;
    var from_folder = $("<div>").html(id);
    if ("media$thumbnail" in a[i]) {
        var result = a[i].media$thumbnail.url;
        var v = result.replace("/s72-c", "/w200");
        var obj = result.replace("/s72-c", "/w100");
        var errorMessage = result.replace("/s72-c", "/w35");
        if (result.match("img.youtube.com")) {
            v = result.replace("/default.", "/mqdefault.");
            obj = result;
            errorMessage = obj
        }
    } else if (id.indexOf("<img") > -1) {
        v = from_folder.find("img:first").attr("src");
        obj = v;
        errorMessage = v
    } else {
        v = noThumbnail.replace("/w480",
            "/w200");
        obj = noThumbnail.replace("/w480", "/w100");
        errorMessage = noThumbnail.replace("/w480", "/w35")
    }
    var Aerial = '<span class="post-thumb" data-image="' + v + '" style="background-image:url(' + errorMessage + ')"/>';
    var AerialWithLabels = '<span class="post-thumb" data-image="' + obj + '" style="background-image:url(' + errorMessage + ')"/>';
    var VALID_IMAGERY_SETS = [Aerial, AerialWithLabels];
    return VALID_IMAGERY_SETS
}
function getPostComments(value, id, post_id) {
    var secondary = value[id].author[0].name.$t;
    var message = value[id].author[0].gd$image.src.replace("/s512-c", "/s55-r");
    var video1title = value[id].title.$t;
    if (message.match("//img1.blogblog.com/img/blank.gif")) var ongoingMessage = "//4.bp.blogspot.com/-oSjP8F09qxo/Wy1J9dp7b0I/AAAAAAAACF0/ggcRfLCFQ9s2SSaeL9BFSE2wyTYzQaTyQCK4BGAYYCw/s55-r/avatar.jpg";
    else ongoingMessage = message;
    var _0x630cx22 = '<article class="custom-item"><a class="post-link" href="' + post_id + '"><div class="post-image cmm-avatar"><span class="post-thumb" data-image="' +
        ongoingMessage + '" style="backfround-image:url(' + ongoingMessage + ')"/></div><h2 class="post-title">' + secondary + '</h2><span class="cmm-snippet">' + video1title + "</span></a></article>";
    return _0x630cx22
}
function getAjax(table, name, id, callback) {
    switch (name) {
    case "msimple":
    case "megatabs":
    case "list":
    case "related":
        if (callback == undefined) callback = "geterror.x5j7a";
        var server = getFeedUrl(name, id, callback);
        $.ajax({
            url: server,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                switch (name) {
                case "related":
                    table.html(beforeLoader()).parent().addClass("show-ify");
                    break;
                case "list":
                    table.html(beforeLoader());
                    break
                }
            },
            success: function (data) {
                var html = "";
                switch (name) {
                case "msimple":
                case "megatabs":
                    html = '<ul class="mega-widget">';
                    break;
                case "list":
                    html = '<div class="custom-widget">';
                    break;
                case "related":
                    html = '<div class="related-posts">';
                    break
                }
                var k = data.feed.entry;
                if (k != undefined) {
                    var i = 0;
                    var id = k;
                    for (; i < id.length; i++) {
                        var url = getPostLink(id, i);
                        var thisM = getPostTitle(id, i);
                        var vertexSet = getPostImage(id, i);
                        var salesTeam = getPostDate(id, i);
                        var childs = "";
                        switch (name) {
                        case "msimple":
                        case "megatabs":
                            childs = childs + ('<div class="mega-item"><div class="mega-content"><a class="post-link" href="' + url + '"><div class="post-image">' + vertexSet[0] +
                                '</div><h2 class="post-title">' + thisM + '</h2><div class="post-meta">' + salesTeam + "</div></a></div></div>");
                            break;
                        case "list":
                            switch (callback) {
                            case "comments":
                                var value = getPostComments(id, i, url);
                                childs = childs + value;
                                break;
                            default:
                                childs = childs + ('<article class="custom-item item-' + i + '"><a class="post-link" href="' + url + '"><div class="post-image">' + vertexSet[1] + '</div><div class="post-header"><h2 class="post-title">' + thisM + '</h2><div class="post-meta">' + salesTeam + "</div></div></a></article>");
                                break
                            }
                            break;
                        case "related":
                            childs = childs + ('<article class="related-item item-' + i + '"><div class="related-item-inner"><a class="post-link" href="' + url + '"><div class="post-image">' + vertexSet[0] + '</div><div class="post-header"><h2 class="post-title">' + thisM + '</h2><div class="post-meta">' + salesTeam + "</div></div></a></div></article>");
                            break
                        }
                        html = html + childs
                    }
                } else switch (name) {
                case "msimple":
                case "megatabs":
                    html = '<ul class="mega-widget">' + msgError() + "</ul>";
                    break;
                default:
                    html = msgError();
                    break
                }
                switch (name) {
                case "msimple":
                    html =
                        html + "</ul>";
                    table.append(html).addClass("msimple");
                    table.find("a:first").attr("href", function (canCreateDiscussions, code) {
                        switch (callback) {
                        case "recent":
                            code = code.replace(code, "/search/?&max-results=" + postPerPage);
                            break;
                        default:
                            code = code.replace(code, "/search/label/" + callback + "?&max-results=" + postPerPage);
                            break
                        }
                        return code
                    });
                    break;
                default:
                    html = html + "</div>";
                    table.html(html);
                    break
                }
                table.find("span.post-thumb").optify()
            }
        })
    }
}
function ajaxMega(table, p, n, sz, begin) {
    if (begin.match("getmega"))
        if (p == "msimple" || p == "megatabs" || p == "mtabs") return getAjax(table, p, n, sz);
        else table.addClass("has-sub mega-menu").append('<ul class="mega-widget">' + msgError() + "</ul>")
}
function ajaxWidget(table, type, value, html, name) {
    if (name.match("getwidget"))
        if (type == "list") return getAjax(table, type, value, html);
        else table.html(msgError())
}
function ajaxRelated(table, n, visual, callback, scopeIn) {
    if (scopeIn.match("getrelated")) return getAjax(table, n, visual, callback)
}
function ajaxInsta(count, element, i) {
    $.ajax({
        url: "https://api.instagram.com/v1/users/self/media/recent",
        type: "get",
        dataType: "json",
        data: {
            access_token: i,
            count: element
        },
        beforeSend: function () {
            count.html(beforeLoader())
        },
        success: function (res) {
            var count_int = '<ul class="insta-widget">';
            var message = res.meta.error_message;
            var items = res.data;
            if (items != undefined) {
                var i = 0;
                for (; i < items.length; i++) {
                    var reLink = items[i].link;
                    var sizedUrl = items[i].images.thumbnail.url;
                    var delta = "";
                    delta = delta + ('<li class="insta-item item-' +
                        i + '"><a class="insta-image" href="' + reLink + '" target="_blank"><span class="insta-thumb" style="background-image:url(' + sizedUrl + ')"/></a></li>');
                    count_int = count_int + delta
                }
            } else count_int = '<span class"insta-error">' + message + "</span>";
            count_int = count_int + "</ul>";
            count.html(count_int)
        }
    })
}
function getInsta(element, group, button, name) {
    if (name.match("getinstagram")) {
        if (group == undefined) group = 6;
        if (button != undefined) return ajaxInsta(element, group, button);
        else element.html(msgError())
    }
}
$(".maggner-blog-post-comments").each(function () {
    var state = commentsSystem;
    var _0x630cx4f = disqus_blogger_current_url;
    var slider_html = '<div id="disqus_thread"/>';
    var IFBox = '<div class="fb-comments" data-width="100%" data-href="' + _0x630cx4f + '" data-numposts="5"></div>';
    var p = "comments-system-" + state;
    switch (state) {
    case "blogger":
        $(this).addClass(p).show();
        break;
    case "disqus":
        (function () {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.async = true;
            script.src = "//" + disqusShortname +
                ".disqus.com/embed.js";
            (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(script)
        })();
        $("#comments").remove();
        $(this).append(slider_html).addClass(p).show();
        break;
    case "facebook":
        $("#comments").remove();
        $(this).append(IFBox).addClass(p).show();
        break;
    case "hide":
        $(this).hide();
        break;
    default:
        $(this).addClass("comments-system-default").show();
        break
    }
});
$(function () {
    $(".index-post .post-image .post-thumb, .PopularPosts .post-image .post-thumb, .FeaturedPost .post-image .post-thumb").optify();
    $("#mm-section").menuify();
    $("#main-mobile-nav").clone().appendTo(".mobile-menu");
    $("#header-social ul.social").clone().appendTo(".social-mobile");
    $(".show-mobile-menu, .hide-mobile-menu, .overlay").on("click", function () {
        $("body").toggleClass("nav-active");
        $(".overlay").fadeToggle(170)
    });
    $(".mobile-menu .has-sub").append('<div class="submenu-toggle"/>');
    $(".mobile-menu ul li .submenu-toggle").on("click",
        function (event) {
            if ($(this).parent().hasClass("has-sub")) {
                event.preventDefault();
                if (!$(this).parent().hasClass("show")) $(this).parent().addClass("show").children(".m-sub").slideToggle(170);
                else $(this).parent().removeClass("show").find("> .m-sub").slideToggle(170)
            }
        });
    $(".maggner-blog-post-comments").each(function () {
        var type = commentsSystem;
        switch (type) {
        case "disqus":
            $("#gpluscomments").remove();
            break;
        case "facebook":
            $("#gpluscomments").remove();
            break
        }
    });
    $(".header-menu-content").each(function () {
        var sticky_menu = $(this);
        if (fixedMenu == true)
            if (sticky_menu.length > 0) {
                var gasSum = $(document).scrollTop();
                var namespace = sticky_menu.offset().top;
                var _ = sticky_menu.height();
                var baseName = 30;
                var middlePathName = namespace + _ + baseName;
                $(window).scroll(function () {
                    var costSum = $(document).scrollTop();
                    var i = $("#footer-wrapper").offset().top;
                    var snI = i - _;
                    if (costSum < snI) {
                        if (costSum > middlePathName) sticky_menu.addClass("fixed-menu");
                        else sticky_menu.removeClass("fixed-menu");
                        if (costSum > gasSum) sticky_menu.removeClass("show-fixed-menu");
                        else sticky_menu.addClass("show-fixed-menu");
                        gasSum = $(document).scrollTop()
                    }
                })
            }
    });
    $("#main-wrapper, #sidebar-wrapper").each(function () {
        if (fixedSidebar ==
            true) $(this).theiaStickySidebar({
            additionalMarginTop: 30,
            additionalMarginBottom: 30
        })
    });
    $(".back-top").each(function () {
        var editActionMapNode = $(this);
        $(window).on("scroll", function () {
            if ($(this).scrollTop() >= 100) editActionMapNode.fadeIn(250);
            else editActionMapNode.fadeOut(250)
        });
        editActionMapNode.click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 500)
        })
    })
});